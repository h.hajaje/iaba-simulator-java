/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simulator;

import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author admin
 */
public class Simulator {

    static int[] N = new int[]{5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 80, 90, 100, 120, 140, 160, 180, 200, 240, 290, 340, 400};
    static int[] dN = new int[]{26, 56, 85, 115, 144, 179, 210, 249, 263, 306, 324, 363, 388, 418, 492, 563, 598, 729, 835, 957, 1083, 1214, 1443, 1745, 2046, 2048};

    public static void main(String[] args) {
        /*
        String[] nameProtocol = new String[]{"SP","ABA","Wopt","IABA"};
        double[][][] stats = new double[nameProtocol.length][N.length][];
        for (int i = 0; i < nameProtocol.length; i++) {
            for (int j = 0; j < N.length; j++) {
                Topology topo = new Topology();
                topo.W = dN[j];
                topo.run(i, N[j]);
                stats[i][j] = Statistic.getStatistic(topo);
            }
        }
        
        toFiles(N, stats, nameProtocol);
        */
        for (int j = 0; j < N.length; j++) {
                Topology topo = new Topology();
                topo.W = dN[j];
                topo.run(4, N[j]);
                System.out.println((Statistic.getProbabilityCollision(topo)*4096+"").replaceAll("\\.", ","));
            }
        /*
        for (int i = 0; i < 4; i++) {
            System.out.println("F=");
            for (int j = 0; j < N.length; j++) {
                Topology topo = new Topology();
                topo.W = dN[j];
                topo.run(i, N[j]);
                double taux = Statistic.getTaux(topo);
                System.out.println((Statistic.getFairness(topo) + "").replaceAll("\\.", ","));
            }
        }
        */
    }

    public static void toFile(int[] N, double[][][] stats, String[] nameProtocol, int stat) throws IOException {
        String[] names = Statistic.arranged();
        FileWriter fw = new FileWriter(names[stat] + ".txt");
        fw.write("\t");
        for (int i = 0; i < nameProtocol.length - 1; i++) {
            fw.write(nameProtocol[i] + "\t");
        }
        fw.write(nameProtocol[nameProtocol.length - 1] + "\n");
        for (int i = 0; i < N.length; i++) {
            fw.write(N[i] + "\t");
            for (int j = 0; j < stats.length - 1; j++) {
                fw.write(stats[j][i][stat] + "\t");
            }
            if (i == N.length - 1) {
                fw.write(stats[stats.length - 1][i][stat] + "");
            } else {
                fw.write(stats[stats.length - 1][i][stat] + "\n");
            }
        }
        fw.close();
    }

    public static void toFiles(int[] N, double[][][] stats, String[] nameProtocol) {
        for (int stat = 0; stat < stats[0][0].length; stat++) {
            try {
                toFile(N, stats, nameProtocol, stat);
            } catch (IOException ex) {
            }
        }
    }
}
