/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simulator;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Topology {

    ArrayList<Node> nodes = new ArrayList<>();
    static int macMaxFrameRetries = 3;
    public static int macMaxCSMABackoffs = 7;
    static int L = 14, minBE = 3, maxBE = 11;
    int tm;
    int cm;
    int cmt;
    boolean Sending;
    int SIM_TIME = 1000000;
    static int TIME = 0;
    static double beta = 0.8;
    int Idle_TIME = 0;
    static int W;

    public void init(int Mod, int n) {
        Idle_TIME = 0;
        tm = 0;
        cm = 0;
        nodes.clear();
        for (int i = 0; i < n; i++) {
            Node N = new Node();
            N.updateW(Mod);
            N.j = (int) (Math.random() * 9999) % N.W;
            if (N.j == 0) {
                N.j = 1;
                N.i = 1;
            }
            nodes.add(N);
        }
    }

    public void run(int Mod, int n) {
        init(Mod, n);
        for (TIME = 0; TIME < SIM_TIME; TIME++) {
            work(Mod);
            if (!Sending) {
                Idle_TIME++;
            }
        }
    }

    public void work(int Mod) {
        ArrayList<Integer> notWorking = new ArrayList<>();
        Sending = false;
        ArrayList<Integer> transmited = new ArrayList<>();
        for (int i = 0; i < nodes.size(); i++) {
            boolean b = nodes.get(i).work();
            if (!b && nodes.get(i).i == 1) {
                notWorking.add(i);
            }
            if (nodes.get(i).i == 2) {
                Sending = true;
                transmited.add(i);
            }
        }

        for (int i = 0; i < notWorking.size(); i++) {
            Node n = nodes.get(notWorking.get(i));
            if (Sending) {
                n.NB++;
                if (n.NB <= macMaxCSMABackoffs) {
                    n.BE = Math.min(n.BE + 1, maxBE);
                } else {
                    n.BE = minBE;
                    n.NB = 0;
                    n.afailure++;
                }
                n.updateW(Mod);
                n.j = (int) (Math.random() * n.W);
                if (n.j == 0) {
                    n.i = 1;
                    n.j = 1;
                } else {
                    n.i = 0;
                }
            } else {
                if (n.j == 1) {
                    n.j--;
                } else {
                    n.i = 2;
                }
            }
        }
        if (!transmited.isEmpty()) {
            for (int i = 0; i < transmited.size(); i++) {
                Node n = nodes.get(transmited.get(i));
                n.updateW(Mod);
                n.BE = minBE;
                n.NB = 0;
                n.j = (int) (Math.random() * n.W);
                if (n.j == 0) {
                    n.j = 1;
                    n.i = 1;
                } else {
                    n.i = 0;
                }
                if (transmited.size() == 1) {
                    n.tm++;
                    n.retries = 0;
                } else {
                    n.cm++;
                    n.retries++;
                    if (n.retries > macMaxFrameRetries) {
                        n.retries = 0;
                        n.cfailure++;
                    }
                }
            }
            if (transmited.size() == 1) {
                tm++;
            } else {
                cm++;
                cmt += transmited.size();
            }
        }
    }
}
