/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simulator;

/**
 *
 * @author admin
 */
public class Statistic {

    public static void claculatePc(Node n) {
        if (n.cm + n.tm != 0) {
            double pc = (1.0 * n.cm) / (n.cm + n.tm);
            n.Pc = Topology.beta * n.Pc + (1.0 - Topology.beta) * pc;
        }
    }

    public static double getFairness(Topology topo) {
        double squareSum = 0.0;
        for (int i = 0; i < topo.nodes.size(); i++) {
            squareSum += Math.pow(topo.L * topo.nodes.get(i).tm, 2);
        }
        return ((double) Math.pow(topo.L * topo.tm, 2)) / (squareSum * topo.nodes.size());
    }

    public static double getEnergie(Topology topo) {
        double Rx = 40, Tx = 30, CCA = 40, Idle = 0.8;
        double energy = 0.0;
        for (int i = 0; i < topo.nodes.size(); i++) {
            energy += topo.L * ((topo.nodes.get(i).tm + topo.nodes.get(i).cm) * Tx + topo.nodes.get(i).tm * Rx) + (topo.nodes.get(i).cca) * CCA + (topo.nodes.get(i).idle-topo.nodes.get(i).tm*topo.L) * Idle;
        }

        energy /= (1000 * topo.nodes.size());
        energy *= (0.32 / 1000);
        return energy;
    }

    public static double getEnergieCollision(Topology topo) {
        double Tx = 30.0;
        return Tx * topo.L * topo.cmt * (0.32 / (1000000 * topo.nodes.size()));
    }

    public static double getReliability(Topology topo) {
        double reliability = 0.0;
        for (int i = 0; i < topo.nodes.size(); i++) {
            reliability += ((1.0 * topo.nodes.get(i).tm) / (topo.nodes.get(i).tm +topo.nodes.get(i).cfailure+ topo.nodes.get(i).afailure));
        }
        return reliability / topo.nodes.size();
    }

    public static double getThroughput(Topology topo) {
        return (1.0 * topo.tm * topo.L) / topo.SIM_TIME;
    }

    public static double getCollisions(Topology topo) {
        return (1.0 * topo.cm * topo.L) / topo.SIM_TIME;
    }

    public static double getDelay(Topology topo) {
        return (1.0 * topo.SIM_TIME) / (topo.tm / topo.nodes.size());
    }

    public static double getIdle(Topology topo) {
        return (1.0 * topo.Idle_TIME) / topo.SIM_TIME;
    }

    public static double getAccessFailures(Topology topo) {
        double af = 0.0;
        double cf = 0.0;
        for (int i = 0; i < topo.nodes.size(); i++) {
            af += topo.nodes.get(i).afailure;
            cf += topo.nodes.get(i).cfailure;
        }
        return af / (topo.tm + af + cf);
    }

    public static double getCollisionFailures(Topology topo) {
        double af = 0.0;
        double cf = 0.0;
        for (int i = 0; i < topo.nodes.size(); i++) {
            af += topo.nodes.get(i).afailure;
            cf += topo.nodes.get(i).cfailure;
        }
        return cf / (topo.tm + af + cf);
    }

    public static double getProbabilityCollision(Topology topo) {
        double pc = 0.0;
        for (int i = 0; i < topo.nodes.size(); i++) {
            pc += (1.0 * topo.nodes.get(i).cm) / (topo.nodes.get(i).tm + topo.nodes.get(i).cm);
        }
        return pc / topo.nodes.size();
    }

    public static double getTaux(Topology topo) {
        double t = 0.0;
        for (int i = 0; i < topo.nodes.size(); i++) {
            t += topo.nodes.get(i).t;
        }
        return t / (topo.nodes.size() * topo.SIM_TIME);
    }

    public static double[] getStatistic(Topology topo) {
        return new double[]{
            getProbabilityCollision(topo),
            getThroughput(topo),
            getIdle(topo),
            getCollisions(topo),
            getDelay(topo),
            getReliability(topo),
            getEnergie(topo),
            getEnergieCollision(topo),
            getFairness(topo)
        };
    }

    public static String[] arranged() {
        return new String[]{"Probability Of Collision","Throughput","Idle","Collision", "Delay",  "Reliability",  "Energie", "Energie Collision",  "Fairness"};
    }
}
