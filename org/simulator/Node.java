/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simulator;

/**
 *
 * @author admin
 */
public class Node {

    int i = 0;
    int j = 0;
    int W = 0;
    int BE = Topology.minBE, NB, retries;
    int tm, cm, cca, idle, cfailure, afailure;
    double Pc = 0.0;
    int t = 0;

    public void updateW(int Mod) {
        switch (Mod) {
            case 0 ->
                W = (int) Math.pow(2, BE);
            case 1 -> {
                Statistic.claculatePc(this);
                W = (int) (Pc * 2048) + 1;
            }
            case 2 ->
                W = Topology.W;
            case 3 -> {
                double Taux = (1.0 * t) / Topology.TIME;
                if (t == 0) {
                    W = 2048;
                } else {
                    double PC = (1.0 * cm) / (cm + tm);
                    int i = 0;
                    double H = 0.004 / (Taux + 0.001) - 0.003;
                    double L = 0.8;

                    double M = (L * H + (1.0 - L) * PC);
                    L = 0.9;
                    int w = (int) (M * 2048);
                    W = (int) ((1 - L) * w + L * W);
                }
            }
            case 4 -> {
                Statistic.claculatePc(this);
                W = (int) (Pc * 4096) + 1;
            }
            default -> {
            }
        }
    }

    public boolean work() {
        if (i == 0) {
            if (j != 0) {
                j--;
            } else {
                i = 1;
                j = 1;
            }
            idle++;
            return true;
        } else {
            t++;
            if (i == 1) {
                cca++;
            }
            if (i == 2 && j < Topology.L - 1) {
                j++;
                return true;
            }
        }
        return false;
    }
}
